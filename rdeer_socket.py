#!/usr/bin/env python3

"""
TODO
- pouvoir faire un truc genre Reindeer-socket --query -l /rdeer/indexes/PF-2009-Bisbal
    pour lister les index
        prendre le basename au lieu de toute le string après le '-l' (index + cwd)
    pour trouver le fos.txt
        si le path est absolu  --> on l'a déja
        si le path est relatif --> il faut ajouter le basedir au chemin (cwd + index + fos.txt)
"""

import sys
import os
import psutil
import tempfile
import argparse
from datetime import datetime
import socket
import pickle
import shutil

import lib.rdeer_common as rdeer


__appname__   = 'rdeer-socket'
__shortdesc__ = 'Short description.'
__licence__   = 'GPL'
__version__   = '0.1.0'
__author__    = 'Benoit Guibert <benoit.guibert@free.fr>'


REINDEER_PROGRAM = "Reindeer-socket"
OUTPUT_BASEDIR = "/tmp/rdeer"
OUTPUT_FILENAME = "reindeer.out"
FOS = 'fos.txt'


def run_server(args, normalize=True):
    """
    Wait request from rdeer-client and
    - redirect request to Reindeer and send modified Reindeer output to rdeer-client
    - list all running indexes
    """
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    conn.bind(('', args.port))
    conn.listen(10)
    ### show start datetime
    timestamp = lambda: datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    print(f"{timestamp()}: Server {socket.gethostname()} "
          f"listening on port {args.port}", file=sys.stdout)

    while True:
        client, addr = conn.accept()

        print(f"-----------\n{timestamp()}", file=sys.stdout)
        ### receive data stream. It won't accept data packet greater than 1024 bytes
        try:
            received = rdeer.recv_msg(client)
            received = pickle.loads(received)
        except pickle.UnpicklingError:
            rdeer.send_msg(client, b"Error: data sent too big.")
            continue
        except EOFError:
            rdeer.send_msg(client, b"Error: ran out of input")
            continue
        except TypeError:
            msg = "Error: no data to send to client (Maybe issue comes from client)."
            print(msg, file=sys.stderr)
            rdeer.send_msg(client, msg.encode())
            continue

        ### Do something according to the type of request
        print('QUERY TYPE: {}'.format(received['type']), file=sys.stdout)
        reindeer = RdeerSRV()
        if received['type'] == 'counts':
            ### send data to Reindeer --query
            # ~ print(f'LENGTH RECEIVED: {len(received)}', file=sys.stdout)
            response = reindeer.query(received)
        elif received['type'] == 'indexes':
            # ~ response = '\n'.join([f"{k} ({i})" for k,i in reindeer.indexes.items()])
            response = '\n'.join([k for k,i in reindeer.indexes.items() if i != 'Loading'])
        else:
            response = f"Error: request type of client is unknown"

        ### Send Error message to rdeer-client
        if response.startswith('Error:'):
            print(response, file=sys.stderr, flush=True)
            rdeer.send_msg(client, response.encode())

        ### Send response to rdeer-client
        rdeer.send_msg(client, response.encode())

    client.close()
    conn.close()



class RdeerSRV:
    """ Interface between Reindeer-socket and rdeer-client """

    def __init__(self):
        """ Class initialiser """
        self.indexes = self.get_index_list()


    def query(self, received):
        """
        Arguments:
            receive: dict('index': <index_name>, 'query': <fasta_file_request>)
        return string of humanized Reindeer query response. If response starts by:
            'Error:' an error has been encountered
            'Loading:' the index is loading (PID but not yet associated TCP PORT)
        """
        ### VARIABLES
        index = received['index']
        query = received['query']
        ## tmp directory and output file
        tmp_dir = tempfile.mkdtemp(prefix="rdeer-", dir='/tmp')
        os.chmod(tmp_dir, 0o755)    # if the user who started 'Reindeer --query' is not the same one.
        output = os.path.join(tmp_dir, 'reindeer.out')
        ## port on witch Reindeer index listen
        try:
            port = self.indexes[index]['port']
            if port == 'Loading':
                return f"Error: Reindeer index '{index}' is now loading, try again later."
        except KeyError:
            return f"Error: Reindeer index '{index}' is not running on '{socket.gethostname()}'."
        reindeer_cwd = self.indexes[index]['cwd']

        ### Connection to Reindeer socket
        conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            conn.connect(('', port))    # Connect to server
        except socket.gaierror:
            return f"Error: unable to connect to {server} on port {port} (might a name resolution issue)."
        except ConnectionRefusedError:
            return f"Error: unable to connect to {server} on port {port} (might a port number issue)."

        ### Reindeer-socket needs fasta file format for query
        query_file = self._set_query_file(query, tmp_dir)
        if query_file.startswith('Error:'):
            self._clean_tmp_files(tmp_dir)
            return query_file

        ### data to send to Reindeer-socket
        conn.send(f"FILE:{query_file}:OUTF:{output}".encode('utf8'))

        ### response from Reindeer-socket
        conn.recv(1024)                 ### first message is useless
        recv = conn.recv(1024)          ### second message contain the name of the file
        type, msg = recv.decode('utf8').rstrip().split(':')

        ### format response
        if type == "DONE":
            print(f"File: {msg}")
            with open(msg) as fh:
                response = ''.join([line[1:] for line in fh.readlines()])
                response = self._humanize(response, received, reindeer_cwd)
        elif type == "ERROR":
            return f"Error: {msg}"

        ### close connexion
        conn.close()

        self._clean_tmp_files(tmp_dir)
        return response.rstrip()


    def start_new_index(self, server, full_index_path):
        ### pick free port number
        # https://stackoverflow.com/questions/1365265/on-localhost-how-do-i-pick-a-free-port-number
        with socketserver.TCPServer(("localhost", 0), None) as s:
            free_port = s.server_address[1]
        ### next ?
        return 'work in progress'


    def stop_index(self, index_name):
        return 'work in progress'


    def get_index_list(self):
        """Return the list of indexes"""
        indexes = {}
        for proc in psutil.process_iter():
            if proc.name() == REINDEER_PROGRAM:
                cmdline = proc.as_dict()['cmdline']
                try:
                    index = cmdline[cmdline.index('-l')+1]
                except ValueError:
                    continue
                try:
                    indexes[index] = {'port': proc.connections()[0].laddr.port, 'cwd': proc.cwd()}
                except IndexError:
                    indexes[index] = {'port': "Loading", 'cwd': proc.cwd()}      # Index has a PID without associated TCP port.
        return indexes


    def get_index_avalaible(self):
        return 'work in progress'


    def _set_query_file(self, query, tmp_dir, query_file='query.fa'):
        """ Reindeer --query needs fasta file format for query """

        query = query.decode()

        ### add newline at last line if not
        try:
            if query[-1] != '\n': query += '\n'
        except IndexError:
            return f"Error: query is empty"

        ### write file
        query_file = os.path.join(tmp_dir, query_file)
        with open(os.path.join(query_file), 'w') as fh:
            fh.write(query)

        ### is file here ?
        if not os.path.isfile(query_file):
            return f"Error: 'Query file {query_file} not found"

        return query_file


    def _humanize(self, response, received, reindeer_cwd):
        """
        1. reduce counts to one number value,
        2. normalize counts (if asked by client)
        """
        index = received['index']
        normalize = received['normalize']
        header = 'seq_name\t'
        samples = []
        kmers_found = []
        ### If response starts by 'Error:', return directly
        if response.startswith('Error:'):
            return response
        ### Add header to data from FOS file (File of Samples)
        try:
            with open(os.path.join(reindeer_cwd, index, FOS)) as fh:
                for line in fh:
                    sample, *kmers = line.split()
                    if kmers: kmers_found.append(kmers[0])
                    samples.append(sample)
                header = header + '\t'.join(samples)
        except FileNotFoundError:
            return f"Error: file {FOS} not found on {args.host or socket.gethostname()}:{os.path.join(reindeer_cwd, index)} location."

        header += '\n'       # Add newline at EOL

        ### Reduce complex values of Reindeer output to single count
        res = []
        for i,line in enumerate(response.split('\n')):  # i --> sequence
            seq_name, *counts = line.split('\t')
            for j,count in enumerate(counts):           # j --> sample/count
                if count != '*':
                    counts[j] = [c.split(':')[1] for c in count.split(",")]
                    ### average of untigs counts - but stars  ('*') must be removed
                    counts[j] = sum([int(c) for c in counts[j] if c != '*']) // len(counts[j])
                    ### NORMALIZE if kmers counts are present in file of samples (fos)
                    if normalize and kmers_found:
                        counts[j] = round(NORM * counts[j] / int(kmers_found[j]),2)
                    elif normalize and not kmers_found:
                        return(f"Error: unable to normalize counts, it could be that {FOS} does not contain counts.")
                    counts[j] = str(counts[j])
                else:
                    counts[j] = '0'
            res.append(seq_name + '\t' + '\t'.join(counts))
        res = '\n'.join(res)

        ### join header and counts
        res = header + res

        return res


    def _clean_tmp_files(self, tmp_dir):
        """ clean temporary file before exit """
        shutil.rmtree(tmp_dir, ignore_errors=True)


def usage():
    """
    Help function with argument parser.
    """
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument("index_dir",
                        help="path to indexes base directory",
                        metavar=('index_dir'),
                        nargs='?',
                       )
    parser.add_argument("-p", "--port",
                        help="port on which Reindeer socket is listening (default: 12800)",
                        metavar="port",
                        default=12801,
                        type=int,
                       )
    # ~ parser.add_argument('-h', '--host',
                        # ~ help="This host, could be IP address or hostname (default: '')",
                        # ~ metavar='host',
                        # ~ default='',
                       # ~ )
    parser.add_argument('-v', '--version',
                        action='version',
                        version=f"{parser.prog} v{__version__}",
                       )
    parser.add_argument('--help',
                        action='help',
                        default=argparse.SUPPRESS,
                        help=argparse._('show this help message and exit')
                        )
    ### Go to "usage()" without arguments
    # if len(sys.argv) == 1:
    #     parser.print_help()
    #     sys.exit()
    return parser.parse_args()


###################################################
#       Launch as program
###################################################

if __name__ == '__main__':
    args = usage()
    run_server(args)
