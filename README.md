# rdeer-service

Very basic client/server implementation, it's actually a POC.

We have to append threading to multiple client at the same time. But in fact, with the ''waiting''
method used with Reindeer, it may be preferable for requests to be queued...

Doc sources to implement python client/server (fr):

- Basic: [openclassroom](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/234698-gerez-les-reseaux) (fr)
- With threading : [python.doctor](https://python.doctor/page-reseaux-sockets-python-port) (fr)
- How to [send a file through a socket](https://askcodez.com/envoyer-un-fichier-via-des-sockets-en-python.html) (fr)

