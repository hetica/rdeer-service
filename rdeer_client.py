#!/usr/bin/env python3

""" Module doc """

import sys
import os
import argparse
import socket
import pickle

sys.path.append(os.path.dirname(os.path.realpath(__file__)))
from lib.rdeer_common import *


__appname__   = "transipedia-client"
__shortdesc__ = "Short description."
__licence__   = "none"
__version__   = "0.2.1"
__author__    = "Benoit Guibert <benoit.guibert@free.fr>"


def main(args):
    """ Function doc """
    if args.index_list:
        # type 'indexes' return running indexes on server
        type = 'indexes'
    else:
        # type 'counts' to query Reindeer
        type = 'counts'
        if sys.stdin.isatty() and not args.index_list:
            with open(args.query) as fh:
                args.query = fh.read()
    # query to rdeer-server
    response = query(
                    type=type,
                    sequences=args.query,
                    index=args.index,
                    normalize=args.normalize,
                    server=args.server,
                    port=args.port,
    )
    print(response.decode())


def query(type, sequences='', index='', normalize=False, server="localhost", port=12800):
    """ Function doc """

    ### Connection to server
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        conn.connect((server, port))    # Connect to server
    except socket.gaierror:
        return f"Error: unable to connect to {server} on port {port} (might a name resolution issue).".encode('utf-8')
    except ConnectionRefusedError:
        return f"Error: unable to connect to {server} on port {port} (might a port number issue).".encode('utf-8')

    ### data to send
    if  type == 'counts':
        to_send = pickle.dumps({'type': type, 'index': index, 'normalize': normalize, 'query': sequences.encode()})
        if __name__ != "__main__":
            print('LENGTH OF DATA TO SEND:', len(to_send))
    elif type == 'indexes':
        to_send = pickle.dumps({'type': type})
    else:
        sys.exit(f"Error: type '{type}' unknown.")
    send_msg(conn, to_send)

    ### received data
    data_recv = recv_msg(conn)

    ### check received file
    data_recv = check_data(data_recv)

    ### close connexion
    conn.close()

    return data_recv


def check_data(data_recv):
    """ Function doc """
    data = data_recv
    ## if rdeer-server still return an error
    if data.startswith(b"Error:"):
        return(data_recv)
    ## if rdeer-server return only header
    n = data.find(b'\n')
    if len(data) <= n+2:
        return b"Error: Rdeer found the index but only header was returned (no counts). " \
                 b"Try again and contact the administrator if the issue still occurs."
    return data_recv


def usage():
    """
    Help function with argument parser.
    https://docs.python.org/3/howto/argparse.html?highlight=argparse
    """
    # Add stdin as argument if command piped
    if not sys.stdin.isatty():
        sys.argv.append(sys.stdin.read())
    index_list_opt = '--index-list'
    # build parser
    parser = argparse.ArgumentParser()
    ### OPTION
    parser.add_argument("query",
                        help="Query file, as fasta format",
                        metavar=('query-file'),
                        default=sys.stdin,
                        nargs=None if index_list_opt not in sys.argv else '?',
                       )
    ### ARGUMENT WITH OPTION
    parser.add_argument("-i", "--index",
                        help="index to request",
                        metavar="index",
                        required=index_list_opt not in sys.argv,
                       )
    parser.add_argument("-s", "--server",
                        help="server hosting index",
                        metavar="server",
                        default='localhost',
                       )
    parser.add_argument("-p", "--port",
                        help="listening tcp port",
                        metavar="port",
                        default=12800,
                        choices=list(range(1,65537)),
                        type=int,
                       )
    parser.add_argument('-n', '--normalize',
                        action="store_true",
                        help="Normalize results, fos.txt must contains counts (see fos_buider.py command)",
                       )
    parser.add_argument(index_list_opt,
                        action="store_true",
                        help="Instead of Reindeer query, return list of running indexes of specified server (default: localhost)"
                       )
    parser.add_argument('-v', '--version',
                        action='version',
                        version=f"{parser.prog} v{__version__}",
                       )
    ### Go to "usage()" without arguments or stdin
    if len(sys.argv) == 1 and sys.stdin.isatty():
        parser.print_help()
        sys.exit()
    return parser.parse_args()


if __name__ == "__main__":
    args = usage()
    main(args)
